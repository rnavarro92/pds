package ConDependenciaInterfaz;

public class Main {
	
	public static void main (String [] args){
		Messi m = new Messi(new Barcelona());
		m.mostrarEquipo();
		//m.setEquipo(new Barcelona());
		
		
		Messi ms = new Messi(new Juventus());
		ms.mostrarEquipo();
		//ms.setEquipo(new Juventus());
		
		Messi mm = new Messi(new Manchester());
		mm.mostrarEquipo();
		//mm.setEquipo(new Manchester());
	}

}
